<?php


namespace Anesh\ShippingAmountAfterOrder\Model\Sorder;

use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\App\Request\DataPersistorInterface;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    protected $dataPersistor;
	protected $_orderFactory;
	protected $_objectManager;
    protected $loadedData;
    protected $collection;


    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        OrderFactory $orderFactory,
		\Magento\Directory\Model\CurrencyFactory $currencyFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
		$this->_orderFactory = $orderFactory;
		$this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $model) {
            $this->loadedData[$model->getId()] = $model->getData();
			$thisOrder = $this->_orderFactory->create();
			$thisOrder->loadByIncrementId($model->getIncrementId());
			if($thisOrder->getStatus() == 'pending'){
				$this->loadedData[$model->getId()]['do_we_hide_it'] = false;
			}else{
				$this->loadedData[$model->getId()]['do_we_hide_it'] = true;
			}
			$orderCurrencyCode = $model->getOrderCurrencyCode();
			$this->loadedData[$model->getId()]['ordered_currency_code'] = $orderCurrencyCode;
			$currency = $this->_objectManager->create('Magento\Directory\Model\CurrencyFactory')->create()->load($orderCurrencyCode);
			$currencySymbol = $currency->getCurrencySymbol();
			$this->loadedData[$model->getId()]['order_currency_symbol'] = $currencySymbol;
			$this->loadedData[$model->getId()]['grand_total_label'] = 'Grand Total '.$currencySymbol;
			$this->loadedData[$model->getId()]['grand_total_notice'] = 'Grand Total is in '.$orderCurrencyCode.'.';
			$this->loadedData[$model->getId()]['shipping_amount_label'] = 'Shipping Amount '.$currencySymbol;
			$this->loadedData[$model->getId()]['shipping_amount_notice'] = 'Shipping amount is in '.$orderCurrencyCode.'. Please enter it in '.$orderCurrencyCode.'.';
        }
        $data = $this->dataPersistor->get('anesh_shippingamountafterorder_sorder');
        
        if (!empty($data)) {
            $model = $this->collection->getNewEmptyItem();
            $model->setData($data);
            $this->loadedData[$model->getId()] = $model->getData();
            $this->dataPersistor->clear('anesh_shippingamountafterorder_sorder');
        }
        
        return $this->loadedData;
    }
}
