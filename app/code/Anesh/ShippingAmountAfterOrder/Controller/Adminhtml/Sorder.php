<?php


namespace Anesh\ShippingAmountAfterOrder\Controller\Adminhtml;

abstract class Sorder extends \Magento\Backend\App\Action
{

    const ADMIN_RESOURCE = 'Anesh_ShippingAmountAfterOrder::top_level';
    protected $_coreRegistry;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE);
        return $resultPage;
    }
}
