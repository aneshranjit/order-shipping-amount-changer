<?php


namespace Anesh\ShippingAmountAfterOrder\Controller\Adminhtml\Sorder;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

	protected $_storeManager;
	
    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->_storeManager = $storeManager;
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('entity_id');
        
            $model = $this->_objectManager->create('Magento\Sales\Model\Order')->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This order no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
        
            try {
                $orderIncrementId = $data['increment_id'];
				
				$orderCurrencyCode = $model->getOrderCurrencyCode();
				$orderCurrencyRate = $this->_storeManager->getStore()->getBaseCurrency()->getRate($orderCurrencyCode);
					$baseCurrencyCode = $model->getBaseCurrencyCode();
					$baseCurrencyRate = $this->_storeManager->getStore()->getBaseCurrency()->getRate($baseCurrencyCode);
				
				$shippingAmount = $data['shipping_amount'];
				$formattedShippingAmount = number_format( floatval($shippingAmount), 4 );
					$baseShippingAmount = $shippingAmount / $orderCurrencyRate;
					$baseFormattedShippingAmount = number_format( floatval($baseShippingAmount), 4 );
				
				$oldShippingAmount = $model->getShippingAmount();
					$baseOldShippingAmount = $model->getBaseShippingAmount();
				
				$oldGrandTotal = $model->getGrandTotal();
					$baseOldGrandTotal = $model->getBaseGrandTotal();
				
				$grandTotal = ($oldGrandTotal - $oldShippingAmount) + $shippingAmount;
				$formattedGrandTotal = number_format( floatval($grandTotal), 4 );
					$baseGrandTotal = ($baseOldGrandTotal - $baseOldShippingAmount) + $baseShippingAmount;
					$baseFormattedGrandTotal = number_format( floatval($baseGrandTotal), 4 );
				
				$model->setShippingAmount($formattedShippingAmount);
					$model->setBaseShippingAmount($baseFormattedShippingAmount);
				
				$model->setShippingInclTax($formattedShippingAmount);
					$model->setBaseShippingInclTax($baseFormattedShippingAmount);
				
				$model->setGrandTotal($formattedGrandTotal);
					$model->setBaseGrandTotal($baseFormattedGrandTotal);
				
				$model->save();
				
				$payment = $model->getPayment();
				
				$payment->setShippingAmount($formattedShippingAmount);
					$payment->setBaseShippingAmount($baseFormattedShippingAmount);
				
				$payment->setAmountOrdered($formattedGrandTotal);
					$payment->setBaseAmountOrdered($baseFormattedGrandTotal);
				
				$payment->save();
				
                $this->messageManager->addSuccessMessage(__('Shipping amount successfully set for this order.'));
                $this->dataPersistor->clear('anesh_shippingamountafterorder_sorder');
        
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while setting the shipping amount.'));
            }
        
            $this->dataPersistor->set('anesh_shippingamountafterorder_sorder', $data);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
